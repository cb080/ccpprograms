#include<stdio.h>
struct employee
{
	char name[20];
	int age;
	float salary;
	float yos;
};	
int main()
{
	struct employee emp;
	printf("Enter the details of the employee:-\n");
	printf("Name :  ");
	gets(emp.name);
	printf("Age: ");
	scanf("%d",&emp.age);
	printf("Salary: ");
	scanf("%f",&emp.salary);
	printf("Years of service: ");
	scanf("%f",&emp.yos);
	printf("\n*******************************************\n");
	printf("The details you have entered is:-\n");
	printf("Name :  ");
	puts(emp.name);
	printf("Age: %d\n",emp.age);
	printf("Salary: %.2f\n",emp.salary);
	printf("Years of service: %.2f",emp.yos);
	return 0;
}